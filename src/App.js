import {useState} from 'react';
import Area from './Components/Area/Area';
import TryAmount from './Components/TryAmount/TryAmount';
import ResetButton from './Components/ResetButton/ResetButton';
import './App.css';

const createStateItems = () => {
  let squares = [];
  const randomIndex = Math.floor(Math.random() * 36);

  for (let i = 0; i < 36; i++) {
    const item = {
      hasItem: false,
      isClicked: false,
      squareClass: ['square-inactive'],
      id: i
    };

    if (i === randomIndex) {
      item.hasItem = true;
    }

    squares.push(item);
  }

  return squares;
};

const App = () => {
  const [squares, setSquares] = useState(createStateItems());
  const [tryAmount, setAmount] = useState(0);
  const [isFinished, setFinished] = useState(false);

  const handleClick = (id) => {
    if (isFinished) {
      alert('Game is over! You should reset the game!');

      return;
    }

    const index = squares.findIndex(s => s.id === id);
    const square = {...squares[index]};

    if (!square.isClicked) {
      square.squareClass.push('square-active');
      square.isClicked = true;
      setAmount(tryAmount + 1);
    }

    if (square.hasItem) {
      setFinished(true);

      setTimeout(function () {
        alert('You have found ring!');
      }, 300);
    }

    const squaresCopy = [...squares];
    squaresCopy[index] = square;

    setSquares(squaresCopy);
  };

  const resetGame = () => {
    setSquares(createStateItems());
    setAmount(0);
    setFinished(false);
  };

  return (
      <div className="App">
        <div className="Area">
          <Area squares={squares} click={handleClick}/>
        </div>

        <TryAmount tryAmount={tryAmount}/>

        <ResetButton resetGame={resetGame}/>
      </div>
  );
}

export default App;
