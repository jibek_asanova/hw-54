import React from 'react';
import './Square.css'

const Square = ({squareClass, click, ring}) => {

    return (
        <div className={squareClass.join(' ')} onClick={click}>
            <span>{ring}</span>
        </div>
    )
};

export default Square;